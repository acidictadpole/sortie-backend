package actor

import akka.actor.Actor
import play.api.Logger
import akka.actor.ActorRef
import akka.actor.Props
import scala.collection.mutable
import com.acidic.sortie.Mission

class LobbyActor(mission: Mission) extends Actor {
  import LobbyActor._
  lazy val logger = Logger(s"LobbyActor")
  val subscribers: mutable.ArrayBuffer[ActorRef] = mutable.ArrayBuffer()
  override def preStart(): Unit = logger.debug("New Mission Creating.")
  def receive: Actor.Receive = {
    case Subscribe(sub) =>
      logger.debug(s"New subscriber.")
      subscribers += sub
      sub ! mission
    case FetchMission() =>
      logger.debug("Mission Data Request")
      sender() ! mission
  }
}

object LobbyActor {
  case class Subscribe(subscriber: ActorRef)
  case class FetchMission()
  def props(mission: Mission) = Props(new LobbyActor(mission))
}