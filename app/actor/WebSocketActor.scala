package actor

import akka.actor._
import akka.pattern.{ ask, pipe }
import play.api.Logger
import akka.util.Timeout
import scala.concurrent.duration._
import play.api.libs.json._
import com.acidic.sortie.Mission

/**
 * The WebSocket Actor represents a single client's (browser) connection to us.
 * Sending messages to the `respond` actor that we are created with, will send
 * websocket messages to the client browser.
 * 
 * @param lobby The Actor responsible for maintaining the mission lobby.
 *              Will usually be an instance of [[LobbyActor]]
 * @param respond The websocket pipe to the client's browser. As an actorRef.
 */
class WebSocketActor(lobby:ActorRef, respond: ActorRef) extends Actor {
  import LobbyActor._
  import Mission._

  lazy val logger = Logger("WebSocketActor")
  //Don't wait longer than 1 second for responses.
  implicit val to = Timeout(1.second)
  //Use our own dispatcher to handle transforming responses.
  implicit val ec = context.dispatcher

  /**
   * This is where messages get received from the browser.
   * They will always come in as strings.
   */
  def receive: Actor.Receive = {
    //This is where messages come in from the client.
    //We can send messages out by 'tell'ing (!) them to `respond`
    case msg: String =>
      logger.debug(s"Got Message [$msg]")
      respond ! s"Ack [$msg]"
  }

  /**
   * When this actor starts up he needs to subscribe to the lobby we're attached to.
   * We also fetch the current lobby's mission state, and send it off to the browser.
   */
  override def preStart(): Unit = {
    //Send the 'Subscribe' message to the lobby actor, with ourselves as the subject.
    //We're also expecting the lobby to respond with the current mission state.
    //We "ask" (?) the lobby actor to subscribe, and then handle its response later.
    (lobby ? Subscribe(self)).mapTo[Mission] map { mission =>
      // The resulting response is a mission. Since we're dealing with websockets
      // we need to translate it into a string to send it.
      val json = Json.toJson(mission)
      Json.prettyPrint(json)
    } pipeTo respond
   }
}

object WebSocketActor {
  //The "constructor" for actors. The Actor model in akka means that we should _never_ have
  //direct references to actors themselves. We, instead, get ActorRef references, and interact with the
  //actor that way.
  def props(lobby: ActorRef, responder: ActorRef) = Props(new WebSocketActor(lobby, responder))
}