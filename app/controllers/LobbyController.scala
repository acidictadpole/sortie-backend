package controllers

import actor.LobbyActor
import actor.WebSocketActor
import akka.actor.ActorSystem
import akka.actor._
import akka.stream.Materializer
import com.acidic.sortie.Mission
import java.{util => ju}
import javax.inject._
import play.api._
import play.api.libs.streams.ActorFlow
import play.api.mvc._
import scala.collection.mutable
import scala.concurrent.Future

/**
 * Responsible for handling the lobby.
 */
@Singleton
class LobbyController @Inject()(cc: ControllerComponents)(implicit system: ActorSystem, mat: Materializer) extends AbstractController(cc) {

  val lobbies: mutable.HashMap[String, ActorRef] = mutable.HashMap()

  def index() = Action { implicit request: Request[AnyContent] =>
    Ok("Hello")
  }

  def newMission = Action { implicit request: Request[AnyContent] =>
    val name = request.body.asFormUrlEncoded.get("name").head
    val mission = Mission(name, "", "", List())
    val missionId = ju.UUID.randomUUID()
    val lobby = system.actorOf(LobbyActor.props(mission))
    lobbies += ((missionId.toString(), lobby))
    Found(s"/mission/${missionId.toString()}")
  }

  def socket(channel: String) = WebSocket.acceptOrResult[String, String] { request =>
    Future.successful(lobbies.get(channel) match {
      case Some(lobbyRef) => {
        Right(ActorFlow.actorRef { out =>
          WebSocketActor.props(lobbyRef, out)
        })
      }
      case None => Left(NotFound)
    })
  }
}

