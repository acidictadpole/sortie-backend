package com.acidic.sortie

import play.api.libs.json._

case class Group(name: String, mission: String, platformType: String, count: Int)

object Group{
  implicit val groupFormat = Json.format[Group]
}

case class Mission(name: String, description: String, owner: String, groups: Seq[Group])

object Mission {
  implicit val missionFormat = Json.format[Mission]
}
