name := """sortie-backend"""
organization := "com.acidic"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.1"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3" % Test
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.5.23" % Test

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.acidic.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.acidic.binders._"
