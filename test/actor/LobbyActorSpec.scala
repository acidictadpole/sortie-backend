package actor

import actor.LobbyActor.Subscribe
import akka.actor.ActorSystem
import akka.testkit.{ TestKit, TestProbe}
import com.acidic.sortie.Mission
import org.scalatest.{ FlatSpecLike, Matchers }
import actor.LobbyActor.FetchMission

class LobbyActorSpec extends TestKit(ActorSystem("LobbyActorSpec")) with FlatSpecLike with Matchers{
  "LobbyActor" should "construct with a mission" in {
    val mission = Mission("foo", "foo", "foo", List())
    system.actorOf(LobbyActor.props(mission))
  }

  it should "respond with the current mission when a new subscriber appears" in {
    val mission = Mission("foo", "foo", "foo", List())
    val lobby = system.actorOf(LobbyActor.props(mission))
    val tp = TestProbe()
    lobby ! Subscribe(tp.ref)
    tp.expectMsg(mission)
  }

  it should "send me the current mission when asked" in {
    val mission = Mission("foo", "foo", "foo", List())
    val lobby = system.actorOf(LobbyActor.props(mission))
    val tp = TestProbe()
    tp.send(lobby, Subscribe(tp.ref))
    tp.expectMsg(mission)
    tp.send(lobby, FetchMission())
    tp.expectMsg(mission)
  }
}