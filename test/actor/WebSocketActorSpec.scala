package actor

import akka.actor.ActorSystem
import akka.testkit.TestKit
import akka.testkit.TestProbe
import com.acidic.sortie.Mission
import org.scalatest.FlatSpec
import org.scalatest.FlatSpecLike
import play.api.libs.json._

class WebSocketActorSpec() extends TestKit(ActorSystem("WebSocketActorSpec")) with FlatSpecLike {
  import LobbyActor._
  "WebSocketActor" should "subscribe to the lobby as soon as it starts up" in {
    val lobby = TestProbe()
    val websocket = TestProbe()
    val websocketactor = system.actorOf(WebSocketActor.props(lobby.ref, websocket.ref))
    lobby.expectMsg(Subscribe(websocketactor))
  }

  it should "send out the current mission from the lobby when it starts up" in {
    val lobby = TestProbe()
    val websocket = TestProbe()
    val websocketactor = system.actorOf(WebSocketActor.props(lobby.ref, websocket.ref))
    lobby.expectMsg(Subscribe(websocketactor))
    val mission = Mission("My Foo Mission", "My Foo Description", "My Foo Owner", List())
    lobby.reply(mission)
    websocket.expectMsg(Json.prettyPrint(Json.toJson(mission)))
  }
}