package models

import org.scalatest.FlatSpec
import com.acidic.sortie.Mission
import org.scalatest.Matchers
import play.api.libs.json._

class MissionSpec extends FlatSpec with Matchers {

  "Missions" should "be able to convert to and from json" in {
    val mission = Mission("MissionName", "Mission Description", "Mission Owner", List())
    val jsonStr: String = Json.prettyPrint(Json.toJson(mission))
    Json.parse(jsonStr).validate[Mission] shouldBe JsSuccess(mission)
  }
}